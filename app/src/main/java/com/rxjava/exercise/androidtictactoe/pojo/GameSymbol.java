package com.rxjava.exercise.androidtictactoe.pojo;

public enum GameSymbol {
    EMPTY, CIRCLE, CROSS
}
