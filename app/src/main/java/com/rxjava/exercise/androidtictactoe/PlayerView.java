package com.rxjava.exercise.androidtictactoe;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.rxjava.exercise.androidtictactoe.pojo.GameSymbol;

public class PlayerView extends ImageView {
    public PlayerView(Context context) {
        super(context);
    }

    public PlayerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PlayerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setData(GameSymbol gameSymbol) {
        switch(gameSymbol) {
            case CIRCLE:
                setImageResource(R.drawable.circle);
                break;
            case CROSS:
                setImageResource(R.drawable.cross);
                break;
            default:
                setImageResource(0);
        }
    }
}
